// MIND MYTH FILMS : server backend : entry point fie
var express = require('express');
var http = require('http');
var fs = require('fs');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();
var port = process.env.PORT || 3000;

var httpServer = http.createServer(app);//creates server, but its not activated by default

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended:false }));

//setup paths
app.use(express.static("routes"));
app.use(express.static("public"));


//routes declaration
app.get('/',
  function(req, res)
  {
    res.render("index.ejs");
  }
);

httpServer.listen(port, ()=>{
  console.log("MIND MYTH FILMS : SERVER STARTED (app) at port : " + port);
});
